import os
import csv
import datetime
from influxdb import client as influxdb

db = influxdb.InfluxDBClient("localhost", 8086, "", "", "prime")#185.233.2.47

def csv_reader(file_obj, file_obj1):
    reader = csv.reader(file_obj, delimiter = ';')
    for row in reader:
        if row[4] == 'Media received':
            if row[5] !='':
                file_obj1.write(row[0] + ';' + row[1] + ';' + row[2] + ';' + row[3][1:] + ';' + row[4] + ';' + row[5] +'\n')
    
    print("Succes parsing file")


def insertDB():
    dirFile = os.path.abspath(os.curdir) + "\\Result\\"
    filesDir = os.listdir(dirFile)
    for filename in filesDir:
        print(filename)
        with open(dirFile + filename, "r") as f_obj:
            reader = csv.reader(f_obj, delimiter = ';')
            for row in reader:
                json_body = [{
                    "measurement": "load",
                    "tags": {
                        "Thread": "User",
                        },
                        "time": '2020-10-21T' + row[1] + 'Z',
                        "fields": {
                            "startTime": row[1],
                            "endTime": row[2],
                            "load": float(row[3]),
                            "chunk": int(row[5]),
                            }
                            }]

                db.write_points(json_body)
        print('succes')
        
if __name__ == "__main__":
    db.query('delete from load')

    listFile = os.listdir()
    print(listFile)
    for filename in listFile:
        if filename.find('.csv') != -1:
            with open(filename, "r") as f_obj:
                with open(os.path.abspath(os.curdir) + "\\Result\\Res " + filename, "w", encoding='UTF-8') as f_obj1:
                    csv_reader(f_obj, f_obj1)  
    
    insertDB()
